package com.stockbit.hiring.di

import com.stockbit.hiring.usecase.TopTierUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { TopTierUseCase(get()) }
}