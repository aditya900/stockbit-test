package com.stockbit.hiring.presentation.ui.watchlist

import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stockbit.common.base.BaseFragment
import com.stockbit.hiring.databinding.FragmentWatchlistBinding
import com.stockbit.hiring.presentation.adapter.TopTierAdapter
import com.stockbit.hiring.presentation.viewmodel.TopTierViewModel
import com.stockbit.model.local.WatchListEntity
import com.stockbit.repository.utils.Resource
import org.koin.android.viewmodel.ext.android.viewModel

class WatchlistFragment : BaseFragment<FragmentWatchlistBinding>() {
    override fun getViewBinding(): FragmentWatchlistBinding =
        FragmentWatchlistBinding.inflate(layoutInflater)

    private val viewModel by viewModel<TopTierViewModel>()

    private val topTierAdapter by lazy {
        TopTierAdapter()
    }

    override fun initView() {
        observeNavigation(viewModel)

        with(binding) {
            this?.rvWatchlistMain?.apply {
                adapter = topTierAdapter
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
            }
            this?.swipteWatchlistMain?.setOnRefreshListener {
                swipteWatchlistMain.isRefreshing = false
                topTierAdapter.submitList(null)
                rvWatchlistMain.visibility = View.GONE
                viewModel.resetValue()
                viewModel.getTopTier()
            }
            this?.scrollWatchlistMain?.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
                    if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight) && viewModel.isPreviousDataLoaded)
                        viewModel.getTopTier(isNextPage = true)
                }
            )
        }

        viewModel.resetValue()
        viewModel.getTopTier()

        with(viewModel) {
            topTier.observe(viewLifecycleOwner) {
                isPreviousDataLoaded = it.status == Resource.Status.SUCCESS
                when (it.status) {
                    Resource.Status.SUCCESS -> onTopTierSuccess(it.data)
                    Resource.Status.LOADING -> {
                        binding?.pbWatchlistMain?.visibility = View.VISIBLE
                        binding?.tvWatchlistLoading?.visibility = View.VISIBLE
                    }
                    Resource.Status.ERROR -> {
                        binding?.pbWatchlistMain?.visibility = View.GONE
                        binding?.tvWatchlistLoading?.visibility = View.GONE
                    }
                }
            }

        }
    }

    private fun onTopTierSuccess(data: List<WatchListEntity>?) {
        with(binding) {
            if (viewModel.isLoadNextPage) {
                this?.tvWatchlistLoading?.visibility = View.GONE
                topTierAdapter.submitList(topTierAdapter.currentList.plus(data.orEmpty()))
            } else {
                this?.pbWatchlistMain?.visibility = View.GONE
                this?.tvWatchlistLoading?.visibility = View.GONE
                if (data?.isNotEmpty() == true) {
                    this?.rvWatchlistMain?.visibility = View.VISIBLE
                    topTierAdapter.submitList(data)
                } else {
                    this?.rvWatchlistMain?.visibility = View.GONE
                }
            }
        }
    }

}