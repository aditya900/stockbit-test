package com.stockbit.hiring.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.stockbit.hiring.databinding.ItemWatchlistBinding
import com.stockbit.model.local.WatchListEntity

class TopTierAdapter :
    ListAdapter<WatchListEntity, TopTierAdapter.ListViewHolder>(TopTierDiffUtilCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TopTierAdapter.ListViewHolder =
        ListViewHolder(
            ItemWatchlistBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: TopTierAdapter.ListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ListViewHolder(private val binding: ItemWatchlistBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: WatchListEntity) {
            with(binding) {
                tvItemWatchlistName.text = data.fullNameCoin
                tvItemWatchlistChange.text = data.changePercent
                tvItemWatchlistInitial.text = data.initialNameCoin
                tvItemWatchlistPrice.text = data.price
            }
        }
    }

    private class TopTierDiffUtilCallback : DiffUtil.ItemCallback<WatchListEntity>() {
        override fun areItemsTheSame(oldItem: WatchListEntity, newItem: WatchListEntity) =
            oldItem.IDWatchList == newItem.IDWatchList

        override fun areContentsTheSame(
            oldItem: WatchListEntity,
            newItem: WatchListEntity
        ) = oldItem == newItem

    }

}