package com.stockbit.hiring.presentation.viewmodel

import com.stockbit.common.base.BaseViewModel
import com.stockbit.hiring.presentation.ui.auth.login.LoginFragmentDirections

class LoginViewModel: BaseViewModel() {
    fun navigateToWatchlist() {
        navigate(LoginFragmentDirections.actFragmentWatchlist())
    }
}