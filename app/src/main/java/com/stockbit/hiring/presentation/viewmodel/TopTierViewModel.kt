package com.stockbit.hiring.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.stockbit.common.base.BaseViewModel
import com.stockbit.hiring.usecase.TopTierUseCase
import com.stockbit.model.local.WatchListEntity
import com.stockbit.repository.utils.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TopTierViewModel(
    private val topTierUseCase: TopTierUseCase
) : BaseViewModel() {
    private val _topTier = MutableLiveData<Resource<List<WatchListEntity>>>()
    val topTier: LiveData<Resource<List<WatchListEntity>>> = _topTier

    var pageNow = 1
    var isPreviousDataLoaded = false
    var isLoadNextPage = false

    fun resetValue() {
        pageNow = 1
    }

    fun getTopTier(isNextPage: Boolean = false) {
        if (isNextPage) pageNow++
        isLoadNextPage = isNextPage

        viewModelScope.launch {
            topTierUseCase.invoke(pageNow).collect { _topTier.postValue(it) }
        }
    }

}