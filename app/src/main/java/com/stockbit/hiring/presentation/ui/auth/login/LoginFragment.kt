package com.stockbit.hiring.presentation.ui.auth.login

import android.text.Editable
import android.text.TextWatcher
import com.stockbit.common.base.BaseFragment
import com.stockbit.hiring.databinding.FragmentLoginBinding
import com.stockbit.hiring.presentation.viewmodel.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<FragmentLoginBinding>(), TextWatcher {

    private val viewModel by viewModel<LoginViewModel>()

    override fun getViewBinding(): FragmentLoginBinding =
        FragmentLoginBinding.inflate(layoutInflater)

    override fun initView() {
        observeNavigation(viewModel)
        with(binding) {
            this?.etAuthEmail?.addTextChangedListener(this@LoginFragment)
            this?.etAuthPassword?.addTextChangedListener(this@LoginFragment)

            this?.btnAuthLogin?.setOnClickListener {
                val email = this.etAuthEmail.text.toString()
                val password = this.etAuthPassword.text.toString()
                val messageEmpty = "Can not be empty"
                when {
                    email != USERNAME -> {
                        etAuthEmail.error = messageEmpty
                    }
                    password != PASSWORD -> {
                        etAuthPassword.error = messageEmpty
                    }
                    else -> {
                        viewModel.navigateToWatchlist()
                    }
                }
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        with(binding) {
            if ((this?.etAuthEmail?.text.toString()
                    .isEmpty()) && (this?.etAuthPassword?.text.toString().isEmpty())
            ) {
                this?.btnAuthLogin?.isEnabled = true
            }
        }
    }

    companion object {
        const val USERNAME = "stockbittest@gmail.com"
        const val PASSWORD = "teststockbit"
    }
}