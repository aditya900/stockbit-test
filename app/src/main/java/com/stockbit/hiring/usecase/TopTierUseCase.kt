package com.stockbit.hiring.usecase

import com.stockbit.repository.TopTierRepository

class TopTierUseCase(private val repository: TopTierRepository) {
    suspend operator fun invoke(page: Int) = repository.getTopTier(20, page, "USD")
}