# Assignment Apps
Applications for technical tests on Bibit and Stockbit, by implementing a clean architecture and paying attention to SOLID PRINCIPLE.

## Tech Stack
- MVVM + Clean Architecture
- Navigation Component
- Single Activity Architecture
- ViewBinding
- Koin for Service Locator
- Coroutines Flow
- Retrofit & OkHttp3
- Room
- Android library modular architecture
- Glide
- Gradle Kotlin.DSL
