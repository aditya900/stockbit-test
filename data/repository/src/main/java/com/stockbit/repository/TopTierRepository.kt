package com.stockbit.repository

import com.stockbit.domain.utils.MapperTopTier
import com.stockbit.local.dao.WatchListDao
import com.stockbit.model.local.WatchListEntity
import com.stockbit.remote.data.RemoteDataSource
import com.stockbit.remote.service.BaseResult
import com.stockbit.repository.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface TopTierRepository {
    suspend fun getTopTier(
        limit: Int,
        page: Int,
        tsym: String
    ): Flow<Resource<List<WatchListEntity>>>
}

class TopTierRepositoryImpl(
    private val datasource: RemoteDataSource,
    private val dao: WatchListDao
) : TopTierRepository {

    override suspend fun getTopTier(
        limit: Int,
        page: Int,
        tsym: String
    ): Flow<Resource<List<WatchListEntity>>> {
        return flow {
            val watchlistDao = dao.getWatchList()

            emit(Resource.success(watchlistDao))

            if (watchlistDao.isNullOrEmpty()) {
                when (val result = datasource.getTopTotalTierVolFull(limit, page, tsym)) {
                    is BaseResult.Error -> emit(Resource.error(Throwable(result.err), null))
                    is BaseResult.Success -> {
                        val listWatchlist = MapperTopTier.topTierResponsesToEntities(result.data)
                        saveToLocal(listWatchlist)
                        emit(Resource.success(listWatchlist))
                    }
                }
            }
        }
    }

    private suspend fun saveToLocal(todos: List<WatchListEntity>) {
        dao.insertWatchList(todos)
    }
}