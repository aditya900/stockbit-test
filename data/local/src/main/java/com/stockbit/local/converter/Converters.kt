package com.stockbit.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stockbit.model.local.WatchListEntity
import java.lang.reflect.Type
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun fromList(list: List<WatchListEntity>?): String? {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun toList(json: String?): List<WatchListEntity>? {
        val listType: Type = object : TypeToken<ArrayList<WatchListEntity>?>() {}.type
        return Gson().fromJson(json, listType)
    }
}