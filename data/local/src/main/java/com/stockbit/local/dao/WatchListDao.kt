package com.stockbit.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.stockbit.model.local.WatchListEntity

@Dao
abstract class WatchListDao : BaseDao<WatchListEntity>() {

    @Query("SELECT * FROM watchlist")
    abstract fun getWatchList(): List<WatchListEntity>?

    fun insertWatchList(data: List<WatchListEntity>) {
        insert(data)
    }
}