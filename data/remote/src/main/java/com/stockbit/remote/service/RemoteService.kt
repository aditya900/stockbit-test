package com.stockbit.remote.service

import com.stockbit.model.remote.watchlist.TopTierResponses
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService {
    @GET("data/top/totaltoptiervolfull")
    suspend fun getTopTotalTierVolFull(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("tsym") tsym: String
    ): Response<TopTierResponses>
}