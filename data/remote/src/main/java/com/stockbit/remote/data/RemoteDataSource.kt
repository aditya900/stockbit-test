package com.stockbit.remote.data

import com.stockbit.model.remote.watchlist.TopTierResponses
import com.stockbit.remote.service.BaseResult
import com.stockbit.remote.service.RemoteService

class RemoteDataSource(private val remoteService: RemoteService) {

    suspend fun getTopTotalTierVolFull(
        limit: Int,
        page: Int,
        tsym: String
    ): BaseResult<TopTierResponses, String> {
        val responses = remoteService.getTopTotalTierVolFull(limit, page, tsym)
        return try {
            val topTierResponses = responses.body()!!
            BaseResult.Success(topTierResponses)
        } catch (e: Exception) {
            BaseResult.Error(responses.message())
        }
    }
}