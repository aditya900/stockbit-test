package com.stockbit.model.remote.watchlist


import com.google.gson.annotations.SerializedName

data class TopTierResponses(
    @SerializedName("Data")
    val `data`: List<Data>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Type")
    val type: Int
)