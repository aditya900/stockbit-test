package com.stockbit.model.remote.watchlist


import com.google.gson.annotations.SerializedName

data class Display(
    @SerializedName("USD")
    val usd: Usd
)