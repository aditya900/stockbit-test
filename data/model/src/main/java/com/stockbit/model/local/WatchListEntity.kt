package com.stockbit.model.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "watchlist")
data class WatchListEntity(
    @PrimaryKey
    @ColumnInfo(name = "IDWatchList")
    var IDWatchList: Int,

    @ColumnInfo(name = "fullNameCoin")
    var fullNameCoin: String,

    @ColumnInfo(name = "price")
    var price: String,

    @ColumnInfo(name = "changePercent")
    var changePercent: String,

    @ColumnInfo(name = "initialNameCoin")
    var initialNameCoin: String,
) {
    constructor() : this(
        -1,
        "",
        "",
        "",
        ""
    )
}
