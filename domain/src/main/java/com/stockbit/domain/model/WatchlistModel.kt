package com.stockbit.domain.model

data class WatchlistModel (
    val fullNameCoin: String,
    val nameCoin: String,
    val price: String,
    val changePercent: String,
)
