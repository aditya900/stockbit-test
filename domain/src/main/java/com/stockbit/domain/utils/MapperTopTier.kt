package com.stockbit.domain.utils

import com.stockbit.model.local.WatchListEntity
import com.stockbit.model.remote.watchlist.TopTierResponses

object MapperTopTier {
    fun topTierResponsesToEntities(input: TopTierResponses): List<WatchListEntity> {
        val topTierList = ArrayList<WatchListEntity>()
        input.data.map {
            val watchListEntity = WatchListEntity(
                IDWatchList = it.coinInfo.id.toInt(),
                fullNameCoin = it.coinInfo.fullName,
                price = it.display.usd.pRICE,
                changePercent = it.display.usd.cHANGEPCTHOUR,
                initialNameCoin = it.coinInfo.internal
            )
            topTierList.add(watchListEntity)
        }
        return topTierList
    }

}